public class Board{

	private Square[][] tictactoeBoard;
	
	Square blank = Square.BLANK;

	public Board(){
		this.tictactoeBoard = new Square[3][3];
	
		this.tictactoeBoard[0][0] = blank;
		this.tictactoeBoard[0][1] = blank;
		this.tictactoeBoard[0][2] = blank;
		
		this.tictactoeBoard[1][0] = blank;
		this.tictactoeBoard[1][1] = blank;
		this.tictactoeBoard[1][2] = blank;
		
		this.tictactoeBoard[2][0] = blank;
		this.tictactoeBoard[2][1] = blank;
		this.tictactoeBoard[2][2] = blank;
	}


	 public String toString(){
		for(int i = 0; i<this.tictactoeBoard.length;i++){
			for(int j = 0;j<this.tictactoeBoard[j].length-1;j++){
				System.out.println();
			}
			System.out.println();
}
			return "";
	}

	public boolean placeToken(int row,int col,Square playerToken){
		if(!(row == 0|| row == 1 || row == 2&& col == 0 || col == 1 || col == 2)){
			return false;
		}
		if(this.tictactoeBoard[col][row] == blank){
			this.tictactoeBoard[col][row] = playerToken;
			return true;
		}
		 return false;
	}
	public boolean checkIfFull(){
	 for(int i = 0; i<this.tictactoeBoard.length;i++){
		 for(int j = 0; j<this.tictactoeBoard[j].length;j++){
			if(this.tictactoeBoard[i][j]==blank){
				return false;
			} 
	}
	 }
	 return true;
	 }
 /*FOR EACH LOOP for CHECKIFFULL but doesnt work public boolean checkIfFull(){
			for(Square[] t:tictactoeBoard){
				if(t.equals( blank)){
				     return false;
				}
					t++;
			}
			return true;
	}*/
 
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[j].length;i++){
		if(this.tictactoeBoard[j].equals(playerToken)){
			return true;
		}
		else{ 
			return false;
		}
		}
	}
	return false;
	}
		
	private boolean checkIfWinningVertical(Square playerToken){
		for(int i = 0; i < this.tictactoeBoard[i].length; i++){
			if(this.tictactoeBoard.equals(playerToken)){
			return true;
			}
			else{
			return false;
			}
	}
	return false;
	}
		
		
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true){
		return true;
		}
		return false;
	}
		}