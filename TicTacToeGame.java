public class TicTacToeGame{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
	
	System.out.println("Hey welcome to the TicTacToe Game!");
	
	Board board = new Board();

	boolean  gameOver = false;
	int  player = 1;
	Square playerToken = Square.X;
	
	while(gameOver == false){
		System.out.println(board);
		
		if(player == 1){
			playerToken = Square.X;
		}
		else{
			playerToken = Square.O;
		}
		System.out.println("Enter the row");
		int row = reader.nextInt();
		System.out.println("Enter the column");
		int col = reader.nextInt();
	    
		 if(board.placeToken(row,col,playerToken) == false){
			 System.out.println("Invalid input. Enter another value");
		 }
		 else if(board.checkIfWinning(playerToken) == true){
			 System.out.println("Player "+player+" is the winner");
			 gameOver = true;
		 }
		 else if(board.checkIfFull() == true){
			 System.out.println("It's a tie!");
			 gameOver = true;
		 }
		 else{
			 System.out.println("it's player 2's turn");
			 player +=1;
		 if(player>2){
			 System.out.println("it's player 1's turn"); 
			 player = 1;
		 }
		 }
		
	}
}
}